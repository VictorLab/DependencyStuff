#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.PackageManager;
using UnityEditor.PackageManager.Requests;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using UnityEngine;

public class DependencyChecker : EditorWindow
{
    static AddRequest Request;
    static RemoveRequest RemoveRequest;
    static ListRequest ListRequest;
    static Dictionary<string, string> RequiredPackages = new Dictionary<string, string>();
    static List<string> PackagesToUpdate = new List<string>();
    static List<string> PackagesToRemove = new List<string>();
    static string packageBeingRemoved;

    // Add a menu item to open the window
    [MenuItem("Window/Dependency Checker")]
    public static void ShowWindow()
    {
        GetWindow<DependencyChecker>("Dependency Checker");
    }

    // Draw the window
    private void OnGUI()
    {
        GUILayout.Label("Packages to Update", EditorStyles.boldLabel);

        foreach (var package in PackagesToUpdate)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(package);
            if (GUILayout.Button("Update"))
            {
                Request = Client.Add(package);
                EditorApplication.update += Progress;
            }
            GUILayout.EndHorizontal();
        }

        GUILayout.Label("Packages to Remove", EditorStyles.boldLabel);

        foreach (var package in PackagesToRemove)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(package);
            if (GUILayout.Button("Remove"))
            {
                packageBeingRemoved = package;
                RemoveRequest = Client.Remove(package);
                EditorApplication.update += RemoveProgress;
            }
            GUILayout.EndHorizontal();
        }

        if (GUILayout.Button("Refresh"))
        {
            CheckForDependencies();
        }
    }

    // Check for dependencies when the script is loaded
    [InitializeOnLoadMethod]
    private static void CheckForDependencies()
    {
        ExtractDependenciesFromAllPackageJsons();
        ListRequest = Client.List();
        EditorApplication.update += CheckPackageList;
    }

    // Extract dependencies from all package.json files in the project
    private static void ExtractDependenciesFromAllPackageJsons()
    {
        // Find all package.json files in the project
        string[] allPackageJsonFiles = Directory.GetFiles(Application.dataPath, "package.json", SearchOption.AllDirectories);

        foreach (string path in allPackageJsonFiles)
        {
            // Read the package.json file and extract the dependencies
            string json;
            try
            {
                json = File.ReadAllText(path);
            }
            catch (FileNotFoundException)
            {
                Debug.LogError("Could not find package.json file at " + path);
                continue;
            }

            var obj = MiniJSON.Json.Deserialize(json) as Dictionary<string, object>;

            if (obj != null && obj.ContainsKey("dependencies"))
            {
                var dependencies = obj["dependencies"] as Dictionary<string, object>;

                if (dependencies != null)
                {
                    foreach (var dependency in dependencies)
                    {
                        // Check if the dependency is already in the list of required packages
                        if (RequiredPackages.ContainsKey(dependency.Key))
                        {
                            // If the dependency is in the list but at a different version, log a warning
                            if (RequiredPackages[dependency.Key] != dependency.Value.ToString())
                            {
                                Debug.LogWarning("Conflict: " + dependency.Key + " is required at different versions");
                                // Resolve the conflict by choosing the latest version
                                string latestVersion = GetLatestVersion(dependency.Key, RequiredPackages[dependency.Key], dependency.Value.ToString());
                                RequiredPackages[dependency.Key] = latestVersion;
                            }
                        }
                        else
                        {
                            // Add the dependency to the list of required packages
                            RequiredPackages.Add(dependency.Key, dependency.Value.ToString());

                            // Recursively check the dependencies of thedependency
                            ExtractDependenciesFromPackageJson(dependency.Key, dependency.Value.ToString());
                        }
                    }
                }
            }
        }
    }

    // Extract dependencies from a specific package.json file
    private static void ExtractDependenciesFromPackageJson(string packageName, string version)
    {
        // Construct the path to the package.json file
        string path = Path.Combine(Application.dataPath, "Packages", packageName, "package.json");

        // Read the package.json file and extract the dependencies
        string json;
        try
        {
            json = File.ReadAllText(path);
        }
        catch (FileNotFoundException)
        {
            Debug.LogError("Could not find package.json file for " + packageName);
            return;
        }

        var obj = MiniJSON.Json.Deserialize(json) as Dictionary<string, object>;

        if (obj != null && obj.ContainsKey("dependencies"))
        {
            var dependencies = obj["dependencies"] as Dictionary<string, object>;

            if (dependencies != null)
            {
                foreach (var dependency in dependencies)
                {
                    // Check if the dependency is already in the list of required packages
                    if (RequiredPackages.ContainsKey(dependency.Key))
                    {
                        // If the dependency is in the list but at a different version, log a warning
                        if (RequiredPackages[dependency.Key] != dependency.Value.ToString())
                        {
                            Debug.LogWarning("Conflict: " + dependency.Key + " is required at different versions");
                            // Resolve the conflict by choosing the latest version
                            string latestVersion = GetLatestVersion(dependency.Key, RequiredPackages[dependency.Key], dependency.Value.ToString());
                            RequiredPackages[dependency.Key] = latestVersion;
                        }
                    }
                    else
                    {
                        // Add the dependency to the list of required packages
                        RequiredPackages.Add(dependency.Key, dependency.Value.ToString());

                        // Recursively check the dependencies of the dependency
                        ExtractDependenciesFromPackageJson(dependency.Key, dependency.Value.ToString());
                    }
                }
            }
        }
    }

    // Compare two versions and return the latest one
    private static string GetLatestVersion(string packageName, string version1, string version2)
    {
        // This is a simplified comparison that might not work for all version formats
        return version1.CompareTo(version2) > 0 ? version1 : version2;
    }

    // Check the list of installed packages
    private static void CheckPackageList()
    {
        if (ListRequest.IsCompleted)
        {
            if (ListRequest.Status == StatusCode.Success)
            {
                foreach (var package in RequiredPackages)
                {
                    if (!IsPackageInstalled(package.Key, package.Value))
                    {
                        // Add the package to the list of packages to update
                        PackagesToUpdate.Add(package.Key + "@" + package.Value);
                    }
                }
            }
            else if (ListRequest.Status >= StatusCode.Failure)
            {
                Debug.Log("Failed to list packages");
            }

            EditorApplication.update -= CheckPackageList;
        }
    }

    // Check if a package is installed and at the correct version
    private static bool IsPackageInstalled(string packageName, string version)
    {
        // Check if the package is installed and at the correct version
        var package = ListRequest.Result.FirstOrDefault(p => p.name == packageName);
        if (package == null)
        {
            return false;
        }
        else if (package.version != version)
        {
            PackagesToRemove.Add(package.name);
            return false;
        }
        else
        {
            return true;
        }
    }

    // Monitor the progress of the package installation
    private static void Progress()
    {
        if (Request.IsCompleted)
        {
            if (Request.Status == StatusCode.Success)
                Debug.Log("Installed: " + Request.Result.packageId);
            else if (Request.Status >= StatusCode.Failure)
                Debug.Log("Failed to install package: " + Request.Result.packageId);

            EditorApplication.update -= Progress;
        }
    }

    // Monitor the progress of the package removal
    private static void RemoveProgress()
    {
        if (RemoveRequest.IsCompleted)
        {
            if (RemoveRequest.Status == StatusCode.Success)
                Debug.Log("Removed: " + packageBeingRemoved);
            else if (RemoveRequest.Status >= StatusCode.Failure)
                Debug.Log("Failed to remove package: " + packageBeingRemoved);

            EditorApplication.update -= RemoveProgress;
        }
    }
}
#endif
